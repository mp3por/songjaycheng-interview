-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.23 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for peachtreebank
DROP DATABASE IF EXISTS `peachtreebank`;
CREATE DATABASE IF NOT EXISTS `peachtreebank` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `peachtreebank`;

-- Dumping structure for table peachtreebank.payments
DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `fromCount` varchar(255) DEFAULT NULL,
  `toCount` varchar(255) DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `kind` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_USER_ID` (`userId`),
  CONSTRAINT `FK_USER_ID` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table peachtreebank.payments: ~6 rows (approximately)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
REPLACE INTO `payments` (`id`, `userId`, `title`, `fromCount`, `toCount`, `amount`, `kind`, `status`, `createdAt`, `updatedAt`) VALUES
	(5, 3, 'DFEF1123', 'AEFDE123', 'DFEF1123', 200, 'card', 'transaction', '2022-01-10 16:21:47', '2022-01-11 20:30:41'),
	(6, 3, 'ccccc', 'aaaaa', 'ccccc', 300, 'online', 'payed', '2022-01-11 02:22:02', '2022-01-11 20:30:49'),
	(7, 3, 'ddddd', 'bbbb', 'ddddd', 234, 'transaction', 'transaction', '2022-01-11 02:22:37', '2022-01-11 20:37:31'),
	(18, 3, 'Test to Account', 'Test from Account', 'Test to Account', 200, 'card', 'received', '2022-01-11 20:36:57', '2022-01-11 20:37:24'),
	(19, 3, 'Test to Account 2', 'Test to Account 2', 'Test to Account 2', 250, 'card', 'payed', '2022-01-11 20:37:19', '2022-01-11 20:37:19'),
	(20, 3, 'Test to Account 3', 'Test from Account 3', 'Test to Account 3', 300, 'card', 'transaction', '2022-01-11 20:37:57', '2022-01-11 20:38:05');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;

-- Dumping structure for table peachtreebank.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table peachtreebank.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `email`, `password`, `createdAt`, `updatedAt`) VALUES
	(3, 'songtest', 'songtest@gmail.com', '$2a$08$1P4DCs7p1lF263udGi3soes2NxE7b/mwfbM/./yUrgP41G3LFfMZa', '2022-01-07 17:41:51', '2022-01-07 17:41:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
