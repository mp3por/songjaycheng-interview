module.exports = (sequelize, Sequelize) => {
  const Payment = sequelize.define("payments", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    userId: {
      type: Sequelize.INTEGER
    },
    title: {
      type: Sequelize.STRING
    },
    fromCount: {
      type: Sequelize.STRING
    },
    toCount: {
      type: Sequelize.STRING
    },
    amount: {
      type: Sequelize.INTEGER
    },
    kind: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.STRING
    }
  });

  return Payment;
};

