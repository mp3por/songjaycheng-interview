const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize('sqlite::memory:');

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model.js")(sequelize, Sequelize);
db.payments = require("./payment.model.js")(sequelize, Sequelize);

db.user.hasMany(db.payments, { as: "payments" });
db.payments.belongsTo(db.user, {
  foreignKey: "userId",
  as: "users",
});

module.exports = db;
