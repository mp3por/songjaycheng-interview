export const BTPaymentState : {[name:string]: string} = {
    payed: 'Payed',
    received: 'Received',
    transaction: 'Send Transaction',
}

export const BTPaymentType : {[name:string]: string} = {
    card: 'Card Payment',
    online: 'Online Transfer',
    transaction: 'Transaction',
}