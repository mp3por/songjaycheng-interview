import { User } from './user.model';
import * as moment from 'moment';

export class Payment {
  id?:  number;
  userId?: number;
  title?: string;
  fromCount?: string;
  toCount?: string;
  amount?: string;
  kind?: string;
  status?: string;
  createdAt?: string;
  updatedAt?: string;
  users?: User;

  constructor(id: number, data: any) {
    this.id = id;
    this.userId = data.query;
    this.title = data.title || '';
    this.fromCount = data.fromCount || '';
    this.toCount = data.toCount || '';
    this.title = data.title || '';
    this.status = data.status || '';
    this.users = data.users;

    if (data.createdAt) {
        this.createdAt = moment(new Date(data.createdAt)).format('MM/DD/YYYY h:mm A');
    } else {
        this.createdAt = moment(new Date()).format('MM/DD/YYYY h:mm A');
    }

    if (data.updatedAt) {
      this.updatedAt = moment(new Date(data.updatedAt)).format('MM/DD/YYYY h:mm A');
    } else {
        this.updatedAt = moment(new Date()).format('MM/DD/YYYY h:mm A');
    }
  }
}

export class PaymentEdit {
  userId?: number;
  title?: string;
  fromCount?: string;
  toCount?: string;
  amount?: string;
  kind?: string;
  status?: string;

  constructor(data: any) {
    this.userId = data.query;
    this.title = data.title || '';
    this.fromCount = data.fromCount || '';
    this.toCount = data.toCount || '';
    this.title = data.title || '';
    this.status = data.status || '';
  }
}
