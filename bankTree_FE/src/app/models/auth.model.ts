export class AuthModel {
  id: number;
  username: string;
  email: string;
  accessToken: string;
}
