import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AdminRoutingModule } from './payment-routing.module';
import { LoginComponent } from './login/login.component';
import { PaymentDetailComponent } from './Payment-detail/payment-detail.component';
import { PaymentComponent } from './payment.component';
import { TranslationModule } from '../i18n/translation.module';
import { InlineSVGModule } from 'ng-inline-svg';
import { PaymentListComponent } from './payment-list/payment-list.component';
import { PaymentCreateComponent } from './payment-create/payment-create.component';
import { NgbDatepickerModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    PaymentComponent,
    LoginComponent,
    PaymentListComponent,
    PaymentDetailComponent,
    PaymentCreateComponent
  ],
  imports: [
    CommonModule,
    TranslationModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    InlineSVGModule,
    InlineSVGModule,
    NgbModalModule,
    NgbDatepickerModule,
    MatSnackBarModule
  ],
  entryComponents: [
  ]
})
export class PaymentModule {}
