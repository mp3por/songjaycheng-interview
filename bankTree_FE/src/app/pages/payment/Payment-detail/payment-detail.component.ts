import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentService } from 'src/app/services/payment.service';
import { Payment } from 'src/app/models/payment.model';
import { BTPaymentState } from 'src/app/models/enumerations';
import { User } from 'src/app/models/user.model';
import { TokenStorageService } from '../../../services/token-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.scss'],
})
export class PaymentDetailComponent implements OnInit {

  payment: Payment;
  user: User;

  loginForm: FormGroup;
  isLoading$: Observable<boolean>;
  btPaymentState: object = BTPaymentState;

  hasError: boolean;
  returnUrl: string;

  id: any;
  state: any;

  private unsubscribe: Subscription[] = []; 

  constructor(
    private paymentService: PaymentService,
    private tokenStorage: TokenStorageService,
    private _Activatedroute: ActivatedRoute,
    private router: Router,
    private _matSnackBar: MatSnackBar,  
    private cdr: ChangeDetectorRef 
  ) {}

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
    this.getPayment(this.id);
  }

  getPayment(id: number): void {
    this.paymentService.get(id)
      .subscribe(
        data => {
          this.payment = data;
          this.state = this.payment.status;
          this.cdr.detectChanges();
        },
        error => {
          console.log(error);
        });
  }

  back(): void {
    this.router.navigate([`/list`]);
  }

  changeState(value): void {
    this.editPayment(value);
  }

  editPayment(value: string): void {
    const data = {
      userId: this.user.id,
      title: this.payment.toCount,
      fromCount: this.payment.fromCount,
      toCount: this.payment.toCount,
      amount: this.payment.amount,
      kind: this.payment.kind,
      status: value
    };

    this.paymentService.update(this.id, data)
      .subscribe(
        response => {
          this.payment = {...this.payment, status: value};
          this.cdr.detectChanges();
          this._matSnackBar.open('Payment state was changed successfully.', 'OK', {
            verticalPosition: 'top',
            duration        : 3000
          });
        },
        error => {
          console.log(error);
          this._matSnackBar.open(error, 'OK', {
            verticalPosition: 'bottom',
            duration        : 3000
          });
        });
  }
} 
