import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Payment } from 'src/app/models/payment.model';
import { PaymentService } from 'src/app/services/payment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { BTPaymentState, BTPaymentType } from 'src/app/models/enumerations';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as _ from 'lodash';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit {

  payments: Payment[];
  currentPayment: any = {};
  currentIndex = -1;
  title = '';
  btPaymentState: object = BTPaymentState;
  btPaymentType: object = BTPaymentType;
  onPayments: BehaviorSubject<Payment[]>;

  constructor(
    private paymentService: PaymentService,
    private route: ActivatedRoute,
    private router: Router,
    private _matSnackBar: MatSnackBar,  
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.retrievePayments();
  }

  retrievePayments() {
    this.paymentService.getAll()
      .subscribe(
        data => {
          this.payments = data;
          this.payments = _.cloneDeep(this.payments);
          this.cdr.detectChanges();
        },
        error => {
          this._matSnackBar.open(error.error.message, 'OK', {
            verticalPosition: 'bottom',
            duration        : 3000
          });
        });
  }

  refreshList() {
    this.retrievePayments();
    this.currentPayment = {};
    this.currentIndex = -1;
  }

  setActivePayment(payment: Payment, index: number): void {
    this.currentPayment = payment;
    this.currentIndex = index;
    this.router.navigate([`/detail/${payment.id}`]);
  }

  removeAllPayments() {
    this.paymentService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle() {
    this.currentPayment = {};
    this.currentIndex = -1;

    this.paymentService.findByTitle(this.title)
      .subscribe(
        data => {
          this.payments = data;
          this.cdr.detectChanges();
        },
        error => {
          this._matSnackBar.open(error.error.message, 'OK', {
            verticalPosition: 'bottom',
            duration        : 3000
          });
        });
  }

  createPayment(value) {
    this.retrievePayments();
  }
}