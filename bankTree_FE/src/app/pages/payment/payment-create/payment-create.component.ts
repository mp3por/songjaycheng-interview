import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { Payment } from 'src/app/models/payment.model';
import { User } from 'src/app/models/user.model';
import { PaymentService } from 'src/app/services/payment.service';
import { TokenStorageService } from '../../../services/token-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-payment-create',
  templateUrl: './payment-create.component.html',
  styleUrls: ['./payment-create.component.scss']
})
export class PaymentCreateComponent implements OnInit {

  @Output() createPayment = new EventEmitter<any>();

  payment: Payment = {};
  submitted = false;
  user: User;

  constructor(
    private paymentService: PaymentService,
    private tokenStorage: TokenStorageService,
    private _matSnackBar: MatSnackBar,   
    private cdr: ChangeDetectorRef
  ) { 
  }

  ngOnInit(): void {
    this.user = this.tokenStorage.getUser();
  }

  savePayment(): void {
    const data = {
      userId: this.user.id,
      title: this.payment.toCount,
      fromCount: this.payment.fromCount,
      toCount: this.payment.toCount,
      amount: this.payment.amount,
      kind: 'card',
      status: 'payed'
    };

    this.paymentService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this._matSnackBar.open('New payment was submitted successfully!', 'OK', {
            verticalPosition: 'top',
            duration        : 3000
          });
          this.payment = {};   
          this.cdr.detectChanges();
          this.createPayment.emit('create');
        },
        error => {
          console.log(error);
          this._matSnackBar.open(error.error.message, 'OK', {
            verticalPosition: 'bottom',
            duration        : 3000
          });
        });
  }
}