import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PaymentComponent} from './payment.component';
import {LoginComponent} from './login/login.component';
import {PaymentDetailComponent} from './Payment-detail/payment-detail.component';
import {PaymentListComponent} from './payment-list/payment-list.component';
import {PaymentCreateComponent} from './payment-create/payment-create.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {returnUrl: window.location.pathname}
      },
      {
        path: 'list',
        component: PaymentListComponent,
        data: {returnUrl: window.location.pathname}
      },
      {
        path: 'detail/:id',
        component: PaymentDetailComponent,
        data: {returnUrl: window.location.pathname}
      },
      {
        path: 'create',
        component: PaymentCreateComponent,
        data: {returnUrl: window.location.pathname}
      },
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: '**', redirectTo: 'login', pathMatch: 'full'},
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AdminRoutingModule {}
